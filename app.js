let firtsNumber;
let secondNumber;
let sign;

const init = () => {
  let result = document.getElementById('result');
  let reset = document.getElementById('reset');
  let addition = document.getElementById('addition');
  let subtraction = document.getElementById('subtraction');
  let multiplication = document.getElementById('multiplication');
  let division = document.getElementById('division');
  let equals = document.getElementById('equals');
  let one = document.getElementById('one');
  let two = document.getElementById('two');
  let three = document.getElementById('three');
  let four = document.getElementById('four');
  let five = document.getElementById('five');
  let six = document.getElementById('six');
  let seven = document.getElementById('seven');
  let eight = document.getElementById('eight');
  let nine = document.getElementById('nine');
  let zero = document.getElementById('zero');

  one.onclick = function(){
    result.textContent = result.textContent  + "1";
  }
  two.onclick = function(){
    result.textContent = result.textContent  + "2";
  }
  three.onclick = function(){
    result.textContent = result.textContent  + "3";
  }
  four.onclick = function(){
    result.textContent = result.textContent  + "4";
  }
  five.onclick = function(){
    result.textContent = result.textContent  + "5";
  }
  six.onclick = function(){
    result.textContent = result.textContent  + "6";
  }
  seven.onclick = function(){
    result.textContent = result.textContent  + "7";
  }
  eight.onclick = function(){
    result.textContent = result.textContent  + "8";
  }
  nine.onclick = function(){
    result.textContent = result.textContent  + "9";
  }
  zero.onclick = function(){
    result.textContent = result.textContent  + "0";
  }
  reset.onclick = function(){
    resetear();
  }
  addition.onclick = function(){
    firtsNumber = result.textContent;
    sign = "+";
    clear();
  }
  subtraction.onclick = function(){
    firtsNumber = result.textContent;
    sign = "-";
    clear();
  }
  multiplication.onclick = function(){
    firtsNumber = result.textContent;
    sign = "*";
    clear();
  }
  division.onclick = function(){
    firtsNumber = result.textContent;
    sign = "/";
    clear();
  }
  equals.onclick = function(){
    secondNumber = result.textContent;
    resolver(firtsNumber, secondNumber, sign);
  }
}

const clear = () => {
  result.textContent = "";
}

const resetear = () => {
  result.textContent = "";
  firtsNumber = 0;
  secondNumber = 0;
  sign = "";
}

const resolver = (firtsNumber, secondNumber, sign) => {
  let res = 0;
  switch(sign){
    case "+":
      res = parseFloat(firtsNumber) + parseFloat(secondNumber);
      break;
    case "-":
        res = parseFloat(firtsNumber) - parseFloat(secondNumber);
        break;
    case "*":
      res = parseFloat(firtsNumber) * parseFloat(secondNumber);
      break;
    case "/":
      res = parseFloat(firtsNumber) / parseFloat(secondNumber);
      break;
  }
  resetear();
  result.textContent = res;
}
